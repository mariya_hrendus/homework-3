use [Mariya Hrendus]
go
insert into resident (first_name, surname, lastname, birthday, blood_group, disease_information, allergy_information, phone_number, note)
values ('Mariya', 'Hrendus', 'Hryhorivna', '1989-01-06', '3+', 'measles', 'no', '0671369408', 'no'),
('Vasyl', 'Hirnyak', 'Hryhorovych', '1980-11-02', '1+', 'chickenpox', 'no', '0672312546', 'no'),
('Olena', 'Matviy', 'Ihorivna', '1991-02-13', '3-', 'cancer', 'no', '0678875342', 'no'),
('Nadiya', 'Helysh', 'Olehivna', '1980-03-06', '2+', 'alvine flux', 'no', '0639567594', 'exempt')
go
select * from resident
go