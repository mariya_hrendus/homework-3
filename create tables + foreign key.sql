use [Mariya Hrendus]
go
create table [resident] (
id int identity primary key NOT NULL,
first_name varchar (20) NOT NULL,
surname varchar (20) NOT NULL,
lastname varchar (20) NULL,
birthday date NOT NULL,
blood_group varchar (20) NOT NULL,
disease_information varchar (30) NOT NULL,
allergy_information varchar (30) NOT NULL,
phone_number varchar (30) NOT NULL,
note varchar (20) NULL,
inserted_date date NOT NULL default getdate(),
updated_date date NULL
)
go
create table [visit](
visit_id int identity primary key NOT NULL,
reason varchar (30) NOT NULL,
visit_date date NOT NULL,
resident_id int NOT NULL,
inserted_date date NOT NULL default getdate(),
updated_date date NULL,
constraint FK_visit_resident FOREIGN KEY (resident_id) 
references resident (id)
)
go 