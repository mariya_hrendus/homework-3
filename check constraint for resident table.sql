use [Mariya Hrendus]
go
alter table resident 
add constraint CK_resident_first_name_surname
check (first_name<>surname and first_name not in ('Magdalena', 'Adam') )